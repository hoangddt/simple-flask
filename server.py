from flask import Flask, request
import random

app = Flask(__name__)

"""
### API:
  * / 
  * /sayhi
  * /get
  * /post
  * /general
  * /temp/<number>
  * /about
"""

@app.route("/")
def hello():
    return "Simple HTTP Server!"

@app.route('/sayhi')
def sayhi():
    """
    Using for get an data from server
    no parameters
    """
    return_msg = "{} Just an random number within (1, 1000)!".format(random.randint(1,1000))
    return return_msg

@app.route('/get', methods=['GET'])
def get():
    """
    Using for sending data from client  to server with GET method
    parameters is sent through url
    example: localhost:5000/get?key=value&name=hoangddt
    """
    return_msg = ''
    for key, val in request.args.items():
        line = "{} : {}<br>".format(key, val)
        return_msg += line
    return_msg = return_msg if return_msg else "Receive nothing"
    return return_msg

@app.route('/post', methods=['POST'])
def post():
    """
    Using for sending data from client to server with GET method
    parameters is sent through POST data (like submiting a form)
    """
    return_msg = ''
    for key, val in request.form.items():
        line = "{} : {}<br>".format(key, val)
        return_msg += line

    return_msg = return_msg if return_msg else "Receive nothing"
    return return_msg

@app.route('/general', methods=['GET', 'POST'])
def general():
    """
    Using for sending data from client to server with both GET/POST method
    parameters is sent through POST data or URL parameters
    """
    return_msg = ''
    if request.method == 'POST':
        return_msg += 'POST<br>'
        for key, val in request.form.items():
            line = "{} : {}<br>".format(key, val)
            return_msg += line
    else:
        return_msg += 'GET<br>'
        for key, val in request.args.items():
            line = "{} : {}<br>".format(key, val)
            return_msg += line 
    return return_msg

@app.route('/temp/<number>')
def temp(number):
    """
    API type, submit an value through endpoint
    Example, submit temperature 35*C to server
    just get: localhost:5000/temp/35
    """
    return "Receive: " + number

@app.route('/about')
def about():
    about = """
    Using for team project at university
    hoangddt, DMT, Truong, Dung
    """
    return about

if __name__ == "__main__":
    app.run(host="0.0.0.0")

